TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef LOCATIONS_H
#define LOCATIONS_H 1
#include "globals.h"

int star_has_loc (int star, location_sort sort);
const char *loc_string (int loc);
int loc_type (int loc, int mask);
int loc_accessible (struct PLAYER *player, int loc);
int risk_level (struct PLAYER *player, int loc);

#endif

