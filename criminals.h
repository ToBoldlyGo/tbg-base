TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CRIMINALS_H
#define CRIMINALS_H 1
#include <stdio.h>
#include "globals.h"

int randomize_criminal (int criminal);
int relocate_criminal (int criminal);
const char * criminal_string (int criminal);
void bounty (FILE * fd, struct PLAYER *player, int loc);
void interrogate (FILE * fd, struct PLAYER *player, int code);
void set_crim (struct PLAYER *player, int crim);
void reset_crim (struct PLAYER *player, int crim);
int get_crim (struct PLAYER *player, int crim);
int find_criminal (int crim);

#endif


