TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ITEMS_H
#define ITEMS_H 1
#include "globals.h"

int find_free_item ();
short new_item (short sort, short efficiency, short reliability, short collection,
                short demo);
short add_item (struct PLAYER *ship, short new);
short generate_item (int tech, int demo);
short find_owner (short item);
void remove_item (int item);
void destroy_item (int item);
struct ITEM *lucky_item (struct PLAYER *player, item_sort sort);
void check_reliability (FILE * fd, struct PLAYER *player);
void repair (FILE * fd, struct PLAYER *player, struct ITEM *item);
void maintain (FILE * fd, struct PLAYER *player, struct ITEM *item);
void priority (FILE * fd, struct PLAYER *player, skill_sort officer);
const char * item_string (struct ITEM *item);
int transfer_item (int item, struct PLAYER *new_owner);
void transmute_items (struct PLAYER *ship, item_sort from, item_sort to);
void generate_ship (struct PLAYER *ship, int tech, int extras, int demo);
void generate_shop (struct PLAYER *ship);
int is_weapon (item_sort sort);
int mass (struct PLAYER *player);
int owner (struct PLAYER *player, int item);
int factor (item_sort, struct PLAYER *);
void destroy_ship (struct PLAYER *ship);
struct ITEM *lucky_item (struct PLAYER *player, item_sort sort);

#endif
