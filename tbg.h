TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TBG_H
#define TBG_H 1
#include "config.h"
extern int want_quiet;
extern int want_verbose;
extern int really_send;
extern int send_email;
extern int turn;
extern int seed;
extern unsigned int session_id;
extern char *desired_directory;
extern char *webroot;
extern char *gameroot;
extern char *server;

#endif
