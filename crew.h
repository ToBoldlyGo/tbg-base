TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CREW_H
#define CREW_H 1
#include "globals.h"
#include <stdio.h>

int ground_combat (FILE * fd, struct PLAYER *player, int level, skill_sort sort,
                   int medical_backup);
int total_pay (struct PLAYER *player);
void check_health (FILE * fd, struct PLAYER *p);
void kill_crew (struct PLAYER *player, skill_sort sort);
void add_crew (struct PLAYER *player, skill_sort skill, int number, int level);
void show_characters (FILE * fd, struct PLAYER *player);
void recruit_rogues (FILE * fd, struct PLAYER *player, int loc);
void heal (FILE * fd, struct PLAYER *player);


#endif
