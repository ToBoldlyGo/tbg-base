TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void
press (int player, const char *text, const char *author)
{
  int rumour;
  char buf1[1024], buf2[1024];
  int s = 0;
  int t = 0;

  memset(buf1, '\0', sizeof(buf1));
  while (text[s])
  {
    switch (text[s])
    {
      case '\"':
        sprintf (buf1, "%s%s", buf1, "&quot;");
        t += 6;
        s++;
        break;
      default:
        buf1[t++] = text[s++];
        break;  
      }
    }
  printf ("%s\n", buf1);
}


int
main ()
{
  char test[] = "Quoted \"test\" string.";

  press(0, test, "PlaceHolder" );
  exit (0);
}

