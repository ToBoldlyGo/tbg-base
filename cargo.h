TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CARGO_H
#define CARGO_H 1

#include "globals.h"
#include <stdio.h>

int any_cargo (struct PLAYER *player, int good);
int unload_pod (FILE * fd, struct PLAYER *player, int good);
int sell (FILE * fd, struct PLAYER *player, byte colony);
int total_cargo (struct ITEM *ship);
void shuffle_goods (struct ITEM *item, int good);
struct ITEM * any_room (struct ITEM *item);
void swap_goods (struct ITEM *ship, struct ITEM *from);
void rationalise_cargo (struct ITEM *item, int scale);
int any_available (struct ITEM *item, int good);
int inner_load_pod (struct ITEM *item, int good, int amount, int depth);
int load_pod (struct ITEM *item, int good, int amount);
void combine_goods (struct ITEM *item, int good);
void init_prices ();
struct ITEM *find_good (struct ITEM *item, int good);

#endif
