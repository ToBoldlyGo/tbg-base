TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef POLITICS_H
#define POLITICS_H 1
#include <stdio.h>

int minister_mod (struct PLAYER *player, int value, item_sort sort);
int aliens_report (int star, int minister, int turn);
void alien_reports ();
void generate_presidential_options (FILE * fd, struct PLAYER *player);
void generate_voting_options (FILE * fd, struct PLAYER *player);
void political_command (struct PLAYER *player, char command, int param);
void make_president_link (FILE * fd, int minister);
void check_votes (FILE * fd, struct PLAYER *player);
void consolidate_votes ();
void do_election ();
void do_tribunal_election ();
void resolve_judgement ();
void influence_decay ();

#endif










