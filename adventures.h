TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ADVENTURES_H
#define ADVENTURES_H
#include <stdio.h>
#include "globals.h"

void set_ad (struct PLAYER *player, int ad);
void reset_ad (struct PLAYER *player, int ad);
int get_ad (struct PLAYER *player, int ad);
void show_adventure (FILE * fd, int adventure);
int try_adventure (FILE * fd, struct PLAYER *player, int adventure);
void update_adventures (struct PLAYER *player);
void show_adventures (FILE *fd, struct PLAYER *player);
void randomize_adventures ();
void reset_adventures ();
void init_adventures ();
void find_adventure (FILE * fd, struct PLAYER *player);




#endif
