TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DYBUK_H
#define DYBUK_H 1

#include <stdio.h>
#include "globals.h"

void logic_bomb (FILE * fd, struct PLAYER *attacker, struct PLAYER *enemy);
void bio_bomb (FILE * fd, struct PLAYER *attacker, struct PLAYER *enemy);
void bangy_bomb (FILE * fd, struct PLAYER *attacker, struct PLAYER *enemy);
int is_evil (struct PLAYER *player);
void banish_evil (FILE *fd, struct PLAYER *evil);
void resolve_evil_interaction (FILE * fd, struct PLAYER *attacker,
                               struct PLAYER *defender);
void resolve_interaction (struct PLAYER *p1, struct PLAYER *p2);
void harvest_popcorn (FILE * fd, struct PLAYER *player);
void do_hiding_damage (FILE * fd, struct PLAYER *player);
void sell_popcorn (FILE * fd, struct PLAYER *player, int amount);
void destabilize (FILE *fd, struct PLAYER *player, int loc);
void reset_popcorn ();
void do_popcorn_auction ();
void bangpedo (FILE * fd, struct PLAYER *attacker,
               struct PLAYER *enemy, int damage);
void biopedo (FILE * fd, struct PLAYER *attacker,
               struct PLAYER *enemy, int damage);
void logipedo (FILE * fd, struct PLAYER *attacker,
               struct PLAYER *enemy, int damage);
void make_evilpedos (FILE *fd, struct PLAYER *player,
                     int num, skill_sort officer);
void generate_evil_options (FILE * fd, struct PLAYER *player,
                            skill_sort officer);
void assign_dybuk ();
void check_stability ();
void check_evil (FILE * fd, struct PLAYER *player);
void update_evil (FILE * fd, struct PLAYER *player);
#endif
