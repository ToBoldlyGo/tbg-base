TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <string.h>
#include "globals.h"
#include "tbg.h"
#include "data.h"
#include "opts.h"

void
remind (int turn, int game)
{
  FILE *fd, *temp;
  char buffer[256];
  int p;

  sprintf (buffer, "%s/remind", desired_directory);
  fd = fopen (buffer, "w");
  if (!fd)
    {
      printf ("Can't create reminder script\n");
      exit (1);
    }
  for (p = 1; p < MAX_PLAYER; p++)
    {
      char *address = players[p].address;
      if (!strcmp (address, "nobody@localhost"))
        continue;
      if (players[p].preferences & 32)
        continue;
      sprintf (buffer, "%s/orders/%d/%s%d",
               webroot, game, players[p].name, turn);
      temp = fopen (buffer, "r");
      if (temp)
        {
          fclose (temp);
          continue;
        }
      //fprintf (fd, "sleep 1\n");
      fprintf (fd, "mail -s \"TBG Reminder - %s\" %s <%s/remind\n",
               players[p].name, address, gameroot);
    }
  fclose (fd);
  sprintf (buffer, "/bin/sh %s/remind", desired_directory);
  system (buffer);
  //printf ("%s\n", buffer);
}

int
main (int agrc, char *argv[])
{
  gameroot="/home/tbg/work";
  desired_directory="/home/tbg/work/tbg";
  webroot="/home/tbg/work/WWW";

  game = 1;
  turn = -1;
  read_master_file ();
  read_data ();
  read_players ();
  remind(turn, game);

  exit(0);
}

