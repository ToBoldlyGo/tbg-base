TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef RELIGION_H
#define RELIGION_H 1


void generate_magic_options (FILE * fd, struct PLAYER *player,
                             skill_sort sort, struct PLAYER *enemy);
void generate_prophet_options (FILE * fd, struct PLAYER *player);
void add_favour (struct PLAYER *player, skill_sort skill, int amount);
void cast_spell (FILE * fd, struct PLAYER *player, int spell, int qualifier);
int give_favour (FILE * fd, skill_sort skill, struct PLAYER *player,
                 struct PLAYER *target);
void check_favour (FILE * fd, struct PLAYER *player);
void commune (FILE * fd, struct PLAYER *player, skill_sort sort);
void choose (FILE * fd, struct PLAYER *player, int target, skill_sort sort);
void write_heretic_lists ();
void write_chosen_lists ();
void show_favour ();


#endif
