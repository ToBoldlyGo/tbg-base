TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SKILL_H
#define SKILL_H 1
#include "globals.h"
#include <stdio.h>

int enlightened (struct PLAYER *player, skill_sort sort);
int skill_level (int skill);
int skill_bit (skill_source sort, int number);
void show_skill (FILE * fd, skill_sort skill, int level);
int effective_skill_level (struct PLAYER *player, skill_sort sort);

#endif
