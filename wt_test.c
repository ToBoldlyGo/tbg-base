TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <sys/types.h>
#include <unistd.h>
#include "bytes.h"
#include "rand.h"

int
weighted_tech ()
{
  int rand_int;

  rand_int = dice (20) + 1;
  switch (rand_int)
    {
    case 1:
    case 2:
    case 3:
    case 4:
      return (1);
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
      return (2);
    case 11:
    case 12:
    case 13:
    case 14:
      return (3);
    case 15:
    case 16:
    case 17:
      return (4);
    case 18:
    case 19:
      return (5);
    case 20:
      return (6);
    default:
      return (1);
    }
}

int
main ()
{
  int i;
  for (i = 0; i < 1000; i++)
    {
    printf ("%d\n", weighted_tech());
    }
  exit (0);
}

