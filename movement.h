TBG, a play by web space opera game
Copyright (C) <2018>  <J. Maiden, E. Moore, and C. Babcock>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MOVEMENT_H
#define MOVEMENT_H 1

#include "globals.h"

int jump_cost (int ship, int s1, int s2);
double inverse_jump_cost (int ship, int c);
void jump (FILE * fd, struct PLAYER *player, int from, int to);
int star_seen (struct PLAYER *p, int star);
void show_destinations (FILE * fd, int self, int current,
                        double min, double max, int showcost);
int square_distance (int s1, int s2);
int distance (int s1, int s2);
int get_random_star (struct PLAYER *player);


#endif
